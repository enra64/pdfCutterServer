#!/bin/bash

array_contains () { 
    local array="$1[@]"
    local seeking=$2
    local in=1
    for element in "${!array}"; do
        if [[ $element == $seeking ]]; then
            in=0
            break
        fi
    done
    return $in
}

input_file=$1
shift

# ignore personalkostenübersicht/übersicht zahlungen
pkue=($(pdfgrep -Hin "Personalkostenübersicht" "$input_file" | cut --delimiter=: --fields=2 --only-delimited -))
uez=($(pdfgrep -Hin "Übersicht Zahlungen" "$input_file" | cut --delimiter=: --fields=2 --only-delimited -))



# iterate over all input arguments, treat each as seperate name
while (( $# > 0 ))
do
    name="$1"                                   # don't understand bash
    # find name occurrences
    occurrences=($(pdfgrep -Hn "$name" "$input_file" | cut --delimiter=: --fields=2 --only-delimited -))
    
    to_be_extracted=()
    
    # print a warning next to occurrences that are believed to be ignore_pages
    for i in "${occurrences[@]}" 
    do
        if array_contains pkue $i
        then
            :
        elif array_contains uez $i
        then
            :
        else
            to_be_extracted+=($i)
        fi
    done
    
    echo $name: ${to_be_extracted[@]}
        
    shift
done


