package exterrors

import "fmt"

// Describes runtime errors within Execute
type ExecutionError struct {
	AdditionalInformation string
	RootError             error
	CommandOutput         string
}

// Format ExecutionErrors to implement the error interface
func (ee ExecutionError) Error() string {
	return fmt.Sprintf("Running the command caused an error; description '%s', root error '%s', output '%s'.",
		ee.AdditionalInformation,
		ee.RootError.Error(),
		ee.CommandOutput,
	)
}
