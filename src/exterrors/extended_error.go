package exterrors

import "fmt"

// Describes runtime errors within Execute
type ExtendedError struct {
	Code                  int
	AdditionalInformation string
	RootError             error
}

// Format ExecutionErrors to implement the error interface
func (ee ExtendedError) Error() string {
	return fmt.Sprintf("Running the command caused an error; Code %d, description '%s', root error '%s'.",
		ee.Code,
		ee.AdditionalInformation,
		ee.RootError.Error(),
	)
}
