package swagger

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"math"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"os"
	"pdfCutterServer/src/responses"
	_ "pdfCutterServer/src/test_helpers"
	"testing"
)

var router = NewRouter()

func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)

	return rr
}

func checkResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}

// Creates a new file upload http request with optional extra params
func newfileUploadRequest(uri string, params map[string]string, paramName, path string) (*http.Request, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	fileContents, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}
	fi, err := file.Stat()
	if err != nil {
		return nil, err
	}
	_ = file.Close()

	body := new(bytes.Buffer)
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile(paramName, fi.Name())
	if err != nil {
		return nil, err
	}
	_, _ = part.Write(fileContents)

	for key, val := range params {
		_ = writer.WriteField(key, val)
	}
	err = writer.Close()
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", uri, body)

	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-Type", writer.FormDataContentType())
	return req, err
}

func TestCutPdf(t *testing.T) {
	extraParams := map[string]string{
		"resultNames": "\"Bert\"|\"Erna\"",
		"pages":       "\"4-2\"|\"2 3\"",
	}
	req, err := newfileUploadRequest("/cutpdf", extraParams, "pdfFile", "./test_files/lohn.pdf")
	if err != nil {
		log.Fatal(err)
	}

	response := executeRequest(req)
	checkResponseCode(t, 200, response.Code)

	targetFile, err := os.Open("./test_files/2 3+4-2.zip")
	if err != nil {
		log.Fatal("Couldn't open test targetFile")
	}
	//noinspection GoUnhandledErrorResult
	defer targetFile.Close()

	fileBytes, err := ioutil.ReadAll(targetFile)

	if err != nil {
		log.Fatal("Couldn't read expected target file")
	}

	responseLength := response.Body.Len()
	targetOriginalLength := len(fileBytes)
	// there is a small variation in the number of output bytes - ignore that.
	if math.Abs(float64(responseLength-targetOriginalLength)) > 10 {
		t.Errorf("Response sourceFile does not have correct size, responded with %s", string(response.Body.Bytes()))
	}

	//// TODO: get a way to make the comparison ignoring the timestamps
	//if !bytes.Equal(response.Body.Bytes(), fileBytes) {
	//	t.Error("Response not equal sourceFile")
	//}
}

func TestGenerateCutSpecification(t *testing.T) {
	extraParams := map[string]string{}
	req, err := newfileUploadRequest("/generateCutSpecification/bruttoNettoThingy/?additionalInformation=Bert%2C%20Erna%2C%20Test", extraParams, "pdfFile", "./test_files/lohn.pdf")
	if err != nil {
		log.Fatal(err)
	}

	response := executeRequest(req)
	checkResponseCode(t, 200, response.Code)

	var generatedCutSpecification = responses.GeneratedCutSpecification{}
	err = json.Unmarshal(response.Body.Bytes(), &generatedCutSpecification)

	if err != nil {
		t.Errorf("Failed to unmarshal response: %s", response.Body.Bytes())
	}
}
