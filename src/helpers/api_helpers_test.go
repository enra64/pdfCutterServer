package helpers

import (
	"net/url"
	"strings"
	"testing"
)

func TestParseCutSpecificationWithNames(t *testing.T) {
	formData := url.Values{}
	formData.Add(resultNamesFormParameterName, "testValue1.pdf|testValue2")
	formData.Add(pagesToCutFormParameterName, "1 2 3 4|1 1 1 1-5")

	if parsedCutSpecs, err := ParseCutSpecification(formData); err != nil {
		t.Error("Got an error during ParseCutSpecification")
	} else {
		if parsedCutSpecs["testValue1.pdf"] != "1 2 3 4" {
			t.Errorf("expected testValue1.pdf to contain 1 2 3 4, but got %s", parsedCutSpecs["testValue1.pdf"])
		}
		if parsedCutSpecs["testValue2.pdf"] != "1 1 1 1-5" {
			t.Errorf("expected testValue2.pdf to contain 1 1 1 1-5, but got %s", parsedCutSpecs["testValue2.pdf"])
		}
		if len(parsedCutSpecs) != 2 {
			t.Error("Didn't get the right number of cut specs")
		}
	}
}

func TestParseCutSpecificationWithoutNames(t *testing.T) {
	formData := url.Values{}
	formData.Add(pagesToCutFormParameterName, "1 2 3 4|1 1 1 1-5 |\" 5-22\"")

	if parsedCutSpecs, err := ParseCutSpecification(formData); err != nil {
		t.Error("Got an error during ParseCutSpecification")
	} else {
		if len(parsedCutSpecs) != 3 {
			t.Error("Didn't get the right number of cut specs")
		}

		for pagesName, pages := range parsedCutSpecs {
			if pagesName == "0.pdf" && pages != "1 2 3 4" {
				t.Error("expected slot 0 to contain 1 2 3 4")
			}
			if pagesName == "1.pdf" && pages != "1 1 1 1-5" {
				t.Error("expected slot 1 to contain 1 1 1 1-5")
			}
			if pagesName == "2.pdf" && pages != "5-22" {
				t.Error("expected slot 2 to contain 5-22")
			}
		}
	}
}

func TestNoFileNamesGiven(t *testing.T) {
	formData := url.Values{}
	resultFileNames := getResultFileNames(formData, 2)
	if len(resultFileNames) != 2 {
		t.Error("didn't get correct number of file names")
	}

	if len(removeDuplicatesUnordered(resultFileNames)) != len(resultFileNames) {
		t.Error("we must not have duplicate file names")
	}

	for _, val := range resultFileNames {
		if val == "" {
			t.Error("file names must not be empty")
		}

		if !strings.HasSuffix(val, ".pdf") {
			t.Error("file names should end in .pdf")
		}
	}
}

func TestAvailableFileNameExtraction(t *testing.T) {
	formData := url.Values{}
	formData.Add(resultNamesFormParameterName, "testValue1.pdf|testValue2.pdf")
	resultFileNames := getResultFileNames(formData, 2)
	if len(resultFileNames) != 2 {
		t.Error("didn't get correct number of file names")
	}

	if len(removeDuplicatesUnordered(resultFileNames)) != len(resultFileNames) {
		t.Error("we must not have duplicate file names")
	}

	for _, val := range resultFileNames {
		if val == "" {
			t.Error("file names must not be empty")
		}

		if !strings.HasSuffix(val, ".pdf") {
			t.Error("file names should end in .pdf")
		}
	}
}

func TestDeduplicateFilenames(t *testing.T) {
	duplicateFileNames := []string{"testFileName.pdf", "testFileName.pdf"}

	deDuplicatedFileNames := deduplicateFileNames(duplicateFileNames)

	if len(duplicateFileNames) != len(deDuplicatedFileNames) {
		t.Error("test array is missing values after deduplication!")
	}

	for _, fileName := range deDuplicatedFileNames {
		if !strings.HasSuffix(fileName, ".pdf") {
			t.Errorf("Ending of %s is missing!", fileName)
		}
	}
}

// from https://www.dotnetperls.com/duplicates-go
func removeDuplicatesUnordered(elements []string) []string {
	encountered := map[string]bool{}

	// Create a map of all unique elements.
	for v := range elements {
		encountered[elements[v]] = true
	}

	// Place all keys from the map into a slice.
	var result []string
	for key := range encountered {
		result = append(result, key)
	}
	return result
}
