package helpers

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"path/filepath"
	"pdfCutterServer/src/responses"
	"strconv"
	"strings"
)

const pagesToCutFormParameterName string = "pages"
const resultNamesFormParameterName string = "resultNames"
const formDataListSep string = "|"
const pdfExtension = ".pdf"

func WriteApiResponse(responseWriter http.ResponseWriter, code int, responseType string, message string, err error) {
	responseWriter.WriteHeader(code)
	responseWriter.Header().Del("Content-Type")
	if err != nil {
		log.Printf("Writing api response, code %d, type %s with message %s", code, responseType, message)
	}

	if b, err := json.Marshal(responses.Status{code, responseType, message}); err == nil {
		fmt.Fprint(responseWriter, string(b))
	} else {
		fmt.Fprintf(responseWriter, "%s", err)
	}
}

func ParseCutSpecification(formData url.Values) (parsedCutSpecs map[string]string, err error) {
	parsedCutSpecs = make(map[string]string)
	if cutSpec, ok := formData[pagesToCutFormParameterName]; !ok {
		return nil, err
	} else {
		cutSpec = strings.Split(cutSpec[0], formDataListSep)
		resultFileNames := getResultFileNames(formData, len(cutSpec))

		for specificationIndex, specification := range cutSpec {
			specification = strings.Trim(specification, "\"")
			specification = strings.Trim(specification, " ")
			parsedCutSpecs[resultFileNames[specificationIndex]] = specification
		}

		return parsedCutSpecs, nil
	}
}

// Extract the result file names given in formData, or defines names itself if no names were given.
func getResultFileNames(formData url.Values, numberOfFiles int) []string {
	resultFileNames := make([]string, numberOfFiles)
	if specifiedNames, hasResultNames := formData[resultNamesFormParameterName]; hasResultNames {
		specifiedNames = strings.Split(specifiedNames[0], formDataListSep)
		for i, specifiedName := range specifiedNames {
			specifiedName = strings.Trim(specifiedName, "\"")
			specifiedName = strings.Trim(specifiedName, " ")

			if !strings.HasSuffix(specifiedName, pdfExtension) {
				specifiedName = specifiedName + pdfExtension
			}

			resultFileNames[i] = specifiedName
		}
		resultFileNames = deduplicateFileNames(resultFileNames)
	} else {
		for i := range resultFileNames {
			resultFileNames[i] = strconv.Itoa(i) + pdfExtension
		}
	}
	return resultFileNames
}

// Append something^TM to any duplicates in the given list of strings to make it unique
func deduplicateFileNames(inputStrings []string) []string {
	deDuplicatedFileNames := make([]string, len(inputStrings))
	duplicationMap := make(map[string]int)
	for i, inputFilename := range inputStrings {
		inputFilenameExtension := filepath.Ext(inputFilename)
		inputFilename = strings.TrimSuffix(inputFilename, inputFilenameExtension)
		if duplicationMap[inputFilename] < 2 {
			deDuplicatedFileNames[i] = fmt.Sprintf(
				"%s%s",
				inputFilename, inputFilenameExtension)
			duplicationMap[inputFilename] = 1
		} else {
			deDuplicatedFileNames[i] = fmt.Sprintf(
				"%s%d%s",
				inputFilename, duplicationMap[inputFilename], inputFilenameExtension)
			duplicationMap[inputFilename] = duplicationMap[inputFilename] + 1
		}
	}
	return deDuplicatedFileNames
}
