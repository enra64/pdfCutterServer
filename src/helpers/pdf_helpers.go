package helpers

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"os"
	"path/filepath"
	"pdfCutterServer/src/exterrors"
	"pdfCutterServer/src/responses"
	"regexp"
	"strings"
	"time"
)

const InvalidCutSpecification = -1
const PdfTkFailed = -2
const NoError = -3

func WriteToDisk(file multipart.File) (string, error) {
	defer file.Close()

	tempFile, err := ioutil.TempFile("", "*.pdf")

	if err != nil {
		return "", err
	}

	bufferedWriter := bufio.NewWriter(tempFile)
	io.Copy(bufferedWriter, file)
	bufferedWriter.Flush()

	tempFilePath, err := filepath.Abs(tempFile.Name())

	if err != nil {
		return "", err
	}

	return tempFilePath, nil
}

func CutPdf(fileLocation string, cutSpecifications map[string]string, deleteSource bool) (outputLocations map[string]string, deleteErr exterrors.ExtendedError) {
	outputLocations = map[string]string{}
	outputFolder := filepath.Join(os.TempDir(), "pdfCutter", "temporaryOutput")
	os.MkdirAll(outputFolder, os.FileMode(0777))

	multiSpaceReplacementRegex := regexp.MustCompile(` {2,}`)
	cutSpecValidifier := regexp.MustCompile(`(?:\d+-\d+|\d+)+`)

	for cutSpecificationName, cutSpecification := range cutSpecifications {

		if filepath.Ext(cutSpecificationName) == "" {
			cutSpecificationName = cutSpecificationName + ".pdf"
		}

		outputLocation := filepath.Join(outputFolder, cutSpecificationName)

		command := []string{"pdftk", fileLocation, "cat"}

		if strings.Contains(cutSpecification, " ") {
			cutSpecification = multiSpaceReplacementRegex.ReplaceAllLiteralString(cutSpecification, "")
			command = append(command, strings.Split(cutSpecification, " ")...)
		} else {
			command = append(command, cutSpecification)
		}
		if !cutSpecValidifier.MatchString(cutSpecification) {
			return nil, exterrors.ExtendedError{InvalidCutSpecification, fmt.Sprintf("The cut specification '%s' is invalid", cutSpecification), nil}
		}

		pdfTkOutput, err := Execute(append(command, "output", outputLocation))

		if err != nil {
			executionError := exterrors.ExecutionError{"pdftk failed", err, pdfTkOutput}
			return nil, exterrors.ExtendedError{PdfTkFailed, executionError.Error(), executionError}
		}

		outputLocations[cutSpecificationName] = outputLocation
	}

	if deleteSource {
		if deleteErr := os.Remove(fileLocation); deleteErr != nil {
			log.Printf("Couldn't delete %s from disk!", fileLocation)
		}
	}

	return outputLocations, exterrors.ExtendedError{NoError, "", nil}
}

func GenerateCutSpecification(fileLocation string, specificationType string, additionalInformation string) (responses.GeneratedCutSpecification, error) {
	switch specificationType {
	case "bruttoNettoThingy":
		return bruttoNettoCutSpecificationGenerator(fileLocation, additionalInformation)
	default:
		return responses.GeneratedCutSpecification{}, errors.New(fmt.Sprintf("Unrecognized specification type %s", specificationType))
	}
}

func bruttoNettoCutSpecificationGenerator(fileLocation string, additionalInformation string) (responses.GeneratedCutSpecification, error) {
	absFileLocation, err := filepath.Abs(fileLocation)
	if err == nil {
		fileLocation = absFileLocation
	}

	additionalInformationParsed := strings.Split(additionalInformation, ",")
	generatorCommand := append([]string{"./src/analyzation_scripts/generateBruttoNettoCutSpecification.sh", fileLocation}, additionalInformationParsed...)

	if generatedOutput, err := Execute(generatorCommand); err == nil {
		var names []string
		var cutSpecs []string

		lines := strings.Split(generatedOutput, "\n")
		for _, line := range lines {
			splitLine := strings.Split(line, ":")
			name := splitLine[0]

			if len(name) > 0 {
				pagesArray := strings.Split(splitLine[1], " ")
				names = append(names, name)
				cutSpecs = append(cutSpecs, strings.Join(pagesArray[1:], " "))
			}
		}

		return responses.GeneratedCutSpecification{time.Now().Unix(), cutSpecs, names}, nil
	} else {
		return responses.GeneratedCutSpecification{}, err
	}
}
