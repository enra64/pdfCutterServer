package helpers

import (
	"os"
	_ "pdfCutterServer/src/test_helpers"
	"testing"
)

func TestCreateZip(t *testing.T) {
	fileLocations := []string{"./test_files/lohn.pdf"}
	outputLocation, err := CreateZip(fileLocations, false)

	if err != nil {
		t.Error("Failed to create the zip - got an error")
	} else {
		// TODO: fix the comparison w/ regard to timestamps
		//cmp := equalfile.New(nil, equalfile.Options{})
		//if isEqual, err := cmp.CompareFile("./test_files/lohn.zip", outputLocation); !isEqual || err != nil {
		//	t.Error("writeToFile failed - files do not equal")
		//}
	}

	_ = os.Remove(outputLocation)
}
