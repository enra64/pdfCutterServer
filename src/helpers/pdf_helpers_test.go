package helpers

import (
	"github.com/udhos/equalfile"
	"log"
	"os"
	"pdfCutterServer/src/responses"
	_ "pdfCutterServer/src/test_helpers"
	"testing"
)

//
//func WritePdfFromRequest(r *http.Request) (string, error) {
//	file, _, err := r.FormFile("pdfFile")
//	defer file.Close()
//
//	if err != nil {
//		return "", err
//	}
//
//	tempFile, err := ioutil.TempFile("", "*.pdf")
//
//	if err != nil {
//		return "", err
//	}
//
//	bufferedWriter := bufio.NewWriter(tempFile)
//	io.Copy(bufferedWriter, file)
//	bufferedWriter.Flush()
//
//	tempFilePath, err := filepath.Abs(tempFile.Name())
//	queryParameters := r.URL.Query()
//
//	queryParametersMap := make(map[string]string)
//	for queryParameterIndex, queryParameter := range queryParameters["pages"] {
//		queryParametersMap[strconv.Itoa(queryParameterIndex)] = queryParameter
//	}
//	return tempFilePath, nil
//}

func TestWriteToDisk(t *testing.T) {
	testFile, err := os.Open("./test_files/lohn.pdf")

	// okay log.Fatal may actually be appropriate for once
	if err != nil {
		log.Fatal(err)
	}

	outputLocation, err := WriteToDisk(testFile)

	if err != nil {
		t.Error("Got an error in WriteToDisk")
	}

	cmp := equalfile.New(nil, equalfile.Options{})
	if isEqual, err := cmp.CompareFile("./test_files/lohn.pdf", outputLocation); !isEqual || err != nil {
		t.Error("writeToFile failed - files do not equal")
	}

	_ = os.Remove(outputLocation)
}

func TestCutPdf(t *testing.T) {
	cutSpecifications := map[string]string{"testA": "1", "testB": "2 3", "testC": "3 2", "testD": "2-4", "testE": "4-2"}
	outputLocations, err := CutPdf(
		"./test_files/lohn.pdf",
		cutSpecifications,
		false,
	)

	if err.Code != NoError {
		t.Errorf("Couldn't cut pdfs, got an error: %s", err.Error())
	}

	// TODO: find a way to actually compare the pdfs. timestamps are different, so hash is ruled out...
	//cmp := equalfile.New(nil, equalfile.Options{})
	//if isEqual, err := cmp.CompareFile("./test_files/1.pdf", outputLocations["testA"]); !isEqual || err != nil {
	//	t.Error("failed comparison for testA")
	//}
	//
	//if isEqual, err := cmp.CompareFile("./test_files/2 3.pdf", outputLocations["testB"]); !isEqual || err != nil {
	//	t.Error("failed comparison for testB")
	//}
	//
	//if isEqual, err := cmp.CompareFile("./test_files/3 2.pdf", outputLocations["testC"]); !isEqual || err != nil {
	//	t.Error("failed comparison for testC")
	//}
	//
	//if isEqual, err := cmp.CompareFile("./test_files/2-4.pdf", outputLocations["testD"]); !isEqual || err != nil {
	//	t.Error("failed comparison for testD")
	//}
	//
	//if isEqual, err := cmp.CompareFile("./test_files/4-2.pdf", outputLocations["testE"]); !isEqual || err != nil {
	//	t.Error("failed comparison for testE")
	//}

	// clean up
	for _, file := range outputLocations {
		_ = os.Remove(file)
	}
}

func TestBruttoNettoCutSpecificationGenerator(t *testing.T) {
	result, err :=
		bruttoNettoCutSpecificationGenerator("./test_files/lohn.pdf", "Bert, Erna, Test")

	verifyBruttoNettoCutSpecificationGeneratorOutput(result, err, t)
}

func TestGenerateCutSpecification(t *testing.T) {
	result, err := GenerateCutSpecification(
		"./test_files/lohn.pdf",
		"bruttoNettoThingy",
		"Bert, Erna, Test",
	)

	verifyBruttoNettoCutSpecificationGeneratorOutput(result, err, t)
}

// Find returns the smallest index i at which x == a[i],
// or -1 if there is no such index.
func find(a []string, x string) int {
	for i, n := range a {
		if x == n {
			return i
		}
	}
	return -1
}

func verifyBruttoNettoCutSpecificationGeneratorOutput(result responses.GeneratedCutSpecification, err error, t *testing.T) {
	if err != nil {
		t.Error("Got an error while analyzing lohn.pdf :(")
	}

	if len(result.CutSpecification) != 3 {
		t.Error("Wrong number of cut specs got back??")
	}

	if len(result.PageNames) != 3 {
		t.Error("Wrong number of page names got back??")
	}

	bertPos := find(result.PageNames, "Bert")
	if bertPos == -1 {
		t.Error("Missing Bert response value!")
	}
	if result.CutSpecification[bertPos] != "2 3" {
		t.Errorf("Expected result for Bert to be '2 3' but it's %s", result.CutSpecification[bertPos])
	}

	ernaPos := find(result.PageNames, "Erna")
	if ernaPos == -1 {
		t.Error("Missing Erna response value!")
	}

	if result.CutSpecification[ernaPos] != "" {
		t.Errorf("Expected result for Erna to be '4' but it's %s", result.CutSpecification[ernaPos])
	}

	testPos := find(result.PageNames, "Test")
	if testPos == -1 {
		t.Error("Missing Test response value!")
	}

	if result.CutSpecification[testPos] != "" {
		t.Errorf("Expected result for Test to be '5' but it's %s", result.CutSpecification[testPos])
	}
}
