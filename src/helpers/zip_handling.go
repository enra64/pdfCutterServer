package helpers

import (
	"archive/zip"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
)

func CreateZip(fileLocations []string, deleteSources bool) (string, error) {
	if tempFile, err := ioutil.TempFile("", "*.zip"); err == nil {
		if zipFileLocation, err := filepath.Abs(tempFile.Name()); err == nil {
			if err = zipFiles(zipFileLocation, fileLocations); err == nil {
				if deleteSources {
					for _, fileLocation := range fileLocations {
						os.Remove(fileLocation)
					}
				}
				return zipFileLocation, nil
			} else {
				return "", err
			}
		} else {
			return "", err
		}
	} else {
		return "", err
	}
}

// zipFiles compresses one or many files into a single zip archive file.
// Param 1: filename is the output zip file's name.
// Param 2: files is a list of files to add to the zip.
func zipFiles(filename string, files []string) error {
	newZipFile, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer newZipFile.Close()

	zipWriter := zip.NewWriter(newZipFile)
	defer zipWriter.Close()

	// Add files to zip
	for _, fileLocation := range files {

		zipFile, err := os.Open(fileLocation)
		if err != nil {
			return err
		}

		// Get the fileLocation information
		info, err := zipFile.Stat()
		if err != nil {
			return err
		}

		header, err := zip.FileInfoHeader(info)
		if err != nil {
			return err
		}

		// Using FileInfoHeader() above only uses the basename of the fileLocation. If we want
		// to preserve the folder structure we can overwrite this with the full path.
		header.Name = filepath.Base(fileLocation)

		// Change to deflate to gain compression
		// see http://golang.org/pkg/archive/zip/#pkg-constants
		header.Method = zip.Deflate

		writer, err := zipWriter.CreateHeader(header)
		if err != nil {
			return err
		}
		if _, err = io.Copy(writer, zipFile); err != nil {
			return err
		}

		if err = zipFile.Close(); err != nil {
			return err
		}
	}
	return nil
}
