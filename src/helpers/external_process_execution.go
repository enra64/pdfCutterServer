package helpers

import (
	"os/exec"
)

// Execute the given command (input as if put into an array by .split(' '))
func Execute(command []string) (string, error) {
	cmd := exec.Command(command[0], command[1:]...)

	if outputBytes, err := cmd.CombinedOutput(); err == nil {
		return string(outputBytes), nil
	} else {
		return string(outputBytes), err
	}
}
