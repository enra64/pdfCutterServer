package test_helpers

import (
	"os"
	"path"
	"runtime"
)

// this function is run when importing _ [...]/test_helpers from anywhere, changing the current working directory
// to the project root, like any godforsaken normal program would do, except for go.
// this is only executed once, no matter how often it is called as the initialization code for this module
func init() {
	testableInit()
}

func testableInit() {
	_, filename, _, _ := runtime.Caller(0)
	dir := path.Join(path.Dir(filename), "../..")
	err := os.Chdir(dir)
	if err != nil {
		panic(err)
	}
}
