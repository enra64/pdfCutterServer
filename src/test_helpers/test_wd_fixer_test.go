package test_helpers

import (
	"log"
	"os"
	"path/filepath"
	"testing"
)

func TestWorkingDirectoryFixer(t *testing.T) {
	testableInit()
	if workingDirectory, err := os.Getwd(); err == nil {
		workingDirectoryBase := filepath.Base(workingDirectory)
		packageName := "pdfCutterServer"

		if workingDirectoryBase != packageName {
			t.Errorf("Working directory '%s' unexpectedly did not end with %s", workingDirectoryBase, packageName)
		}
	} else {
		log.Fatal("Couldn't get working directory")
	}
}
