FROM phusion/baseimage:latest
MAINTAINER enra64 <sujomear@arcor.de>
CMD ["/sbin/my_init"]

ENV PDFTK_VERSION 2.02
RUN apt update && \
    apt install -y --no-install-recommends unzip build-essential gcj-jdk pdfgrep git && \
    apt clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ADD https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/pdftk-${PDFTK_VERSION}-src.zip /tmp/
RUN unzip /tmp/pdftk-${PDFTK_VERSION}-src.zip -d /tmp && \
    sed -i 's/VERSUFF=-4.6/VERSUFF=-5/g' /tmp/pdftk-${PDFTK_VERSION}-dist/pdftk/Makefile.Debian && \
    cd /tmp/pdftk-${PDFTK_VERSION}-dist/pdftk && \
    make -f Makefile.Debian && \
    make -f Makefile.Debian install && \
    rm -Rf /tmp/pdftk-*



ENV GOLANG_VERSION 1.11.1
ADD https://dl.google.com/go/go${GOLANG_VERSION}.linux-amd64.tar.gz /tmp/
RUN cd /tmp && \
    echo 2871270d8ff0c8c69f161aaae42f9f28739855ff5c5204752a8d92a1c9f63993 go${GOLANG_VERSION}.linux-amd64.tar.gz | sha256sum -c - && \
    tar -xvf go${GOLANG_VERSION}.linux-amd64.tar.gz && \
    chown -R root:root ./go && \
    mv go /usr/local && \
    mkdir -p /opt/go/src/pdfCutterServer

ENV PATH="/usr/local/go/bin:${PATH}"
ENV GOPATH="/opt/go"
RUN mkdir /etc/service/pdfcutterserver
COPY pdfCutterServerStart.sh /etc/service/pdfcutterserver/run
RUN chmod +x /etc/service/pdfcutterserver/run

WORKDIR ${GOPATH}/src/pdfCutterServer
COPY . .

RUN go get -d -t -v ./...
RUN go install -v ./...
