# PDF Cutter Server
[![pipeline status](https://gitlab.com/enra64/pdfCutterServer/badges/master/pipeline.svg)](https://gitlab.com/enra64/pdfCutterServer/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/enra64/pdfCutterServer)](https://goreportcard.com/report/gitlab.com/enra64/pdfCutterServer)
[![GoDoc](https://godoc.org/gitlab.com/enra64/pdfCutterServer?status.svg)](http://godoc.org/gitlab.com/enra64/pdfCutterServer)
[![Coverage](https://gitlab.com/enra64/pdfCutterServer/badges/master/coverage.svg)](https://gitlab.com/enra64/pdfCutterServer/commits/master)

This is an open-source server written to facilitate cutting PDF files for users that might not want to trust their
files to some free online service or a random downloaded PDF editor.

## Capabilities
* `pdftk ` wrapper: send pdftk cut specifications with a pdf and get the cut files back
* cut specification generator for when you get german brutto/netto abrechnungen for multiple people in a single pdf. don't ask.

## API
Please check with the api/swagger.yaml file.
